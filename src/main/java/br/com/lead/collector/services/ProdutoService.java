package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto){
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodos(){
        Iterable<Produto> produtos= produtoRepository.findAll();
        return produtos;
    }


    public Iterable<Produto> buscarTodosQueContenhamNome(String nome){
        Iterable<Produto> produtos = produtoRepository.findAllByNomeContaining(nome);
        return produtos;
    }

    public Produto buscarPorId(int id){
        Optional<Produto> optionalLead = produtoRepository.findById(id);
        if(optionalLead.isPresent()){
            return optionalLead.get();
        }
        throw new RuntimeException("O produto não foi encontrado");
    }

    public Produto atualizarProduto(int id, Produto produto){

        if(produtoRepository.existsById(id)){
            produto.setId(id);
            Produto produtoOjeto = salvarProduto(produto);
            return produtoOjeto;
        }
        throw new RuntimeException("O produto não foi encontrado");
    }

    public void deletarProduto(int id){
        if(produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("O produto não foi encontrado");
        }
    }

}
