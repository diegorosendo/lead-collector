package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    public Lead salvarLead(Lead lead){
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodos(){
        Iterable<Lead>  leads= leadRepository.findAll();
        return leads;
    }


    public Iterable<Lead> buscarTodosPorTipoLead(TipoLeadEnum tipoLeadEnum){
        Iterable<Lead> leads = leadRepository.findAllByTipoLead(tipoLeadEnum);
        return leads;
    }

    public Lead buscarPorId(int id){
        Optional<Lead> optionalLead = leadRepository.findById(id);
        if(optionalLead.isPresent()){
            return optionalLead.get();
        }
        throw new RuntimeException("O lead não foi encontrado");
    }

    public Lead atualizarLead(int id, Lead lead){
//        buscarPorId(id);

        if(leadRepository.existsById(id)){
            lead.setId(id);
            Lead leadOjeto = salvarLead(lead);
            return leadOjeto;
        }
        throw new RuntimeException("O lead não foi encontrado");
    }

    public void deletarLead(int id){
        if(leadRepository.existsById(id)){
            leadRepository.deleteById(id);
        } else {
            throw new RuntimeException("O lead não foi encontrado");
        }
    }



}
